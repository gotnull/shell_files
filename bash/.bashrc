[ -f /usr/local/share/bash-completion/bash_completion.sh ] && source /usr/local/share/bash-completion/bash_completion.sh

# ENVIRONMENT VARIABLES
export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTIGNORE="su:ll:la:ld"
export HISTCONTROL=ignoreboth:erasedups:ignorespace

export TERM='xterm-256color'
export LANG=fr_FR.UTF-8

export VISUAL="$(which nvim)"
export EDITOR="$(which nvim)"
export PAGER="$(which less)"

export MANWIDTH=100
export MANPATH=":/usr/share/man:/usr/local/man:/usr/local/share/man"
export MANPAGER="$(which less)"


# ALIAS
alias ppi='pkg install'      
alias pps="pkg search"
alias ppu='pkg update'
alias ppg='pkg upgrade'
alias ppr='pkg delete'
alias ppl='pkg info -l'
alias ppo='pkg info'
alias ppm='pkg info --pkg-message'
alias ppoo='pkg search -D'
alias maj='ppu && ppg'

alias l='ls -1 --color=auto -F'
alias ll='ls -lh --color=auto -F'
alias la='ls -lha --color=auto -F'
alias lt='ls -lhrt --color=auto -F'
alias ld='ls -ld --color=auto -F'

alias cp='cp -v'
alias rm='rm -v'
alias rmf='rm -f -v'
alias rmd='rm -rf -v'
alias mv='mv -v'
alias bc='bc -q -l'
alias df='df -h'
alias du='du -hs'
alias v='$EDITOR'
alias cd-='cd -'
alias ping='ping -v -4 -c2'
alias ,='cd ../'
alias ,,='cd ../..'
alias ,,,='cd ../../..'
alias eng_on='export LC_ALL=C'
alias eng_off='unset LC_ALL'

# FUNCTIONS
src() {
	source "$HOME"/.bashrc
}
qk() {
	ps -aux | sed 1d | fzy --prompt="::> " --line=25 | awk '{print $2}' | xargs kill -9
}

# PROMPT
TXTGRE='\[\e[1;32m\]'
TXTRST='\[\e[0m\]'  
TXTCYN='\[\e[1;36m\]'
TXTPUR='\[\e[1;35m\]'
TXTBLU='\[\e[1;34m\]'
TXTYEL='\[\e[1;33m\]'
TXTRED='\[\e[1;31m\]'
TXTWHI='\[\e[1;37m\]'
TXTBLA='\[\e[1;30m\]'

if [ "$(id -u)" -eq 0 ] ; 
then PS1="${TXTRED}ext:${TXTRST} ${TXTCYN}\u@\h \W ${TXTRED}> ${TXTRST}"
else PS1="${TXTGRE}ext:${TXTRST} ${TXTCYN}\u@\h \W ${TXTGRE}> ${TXTRST}"
fi

# OPTIONS
shopt -s cmdhist
shopt -s histappend


bind 'TAB:menu-complete'
eval "$(zoxide init bash)"

