# $FreeBSD$
# more examples available at /usr/share/examples/csh/

alias h		history 50
alias j		jobs -l
alias lf	ls -FA
alias ll	ls -lhAF
alias v		vim
alias pki	pkg install
alias pks	pkg search
alias pko	pkg info
alias pku	pkg update 
alias pko	pkg info
alias pkoo	pkg search -D

if ( -X nano ) then
	setenv EDITOR nano
else if ( -X vim ) then
	setenv   EDITOR   vim
	alias v  vim
else if (-X vi) then
	setenv   EDITOR   vi
endif

setenv	PAGER	less

set filec
set history = 2000
set histfile = ~/.csh_history
set savehist = (2000 merge)
set histdup = erase
set autolist = ambiguous

set autoexpand
set autorehash
set mail = (/var/mail/$USER)

bindkey "^W" backward-delete-word
bindkey -k up history-search-backward
bindkey -k down history-search-forward
bindkey "\e[1~" beginning-of-line # Home
bindkey "\e[7~" beginning-of-line # Home rxvt
bindkey "\e[2~" overwrite-mode    # Ins
bindkey "\e[3~" delete-char       # Delete
bindkey "\e[4~" end-of-line       # End
bindkey "\e[8~" end-of-line       # End rxvt
bindkey "^[[1;5D" backward-word   # ctrl+left arrow 
bindkey "^[[1;5C" forward-word    # ctrl+right arrow
bindkey "\e[1;3D" backward-word   # alt+left arrow 
bindkey "\e[1;3C" forward-word    # alt+right arrow

setenv MANCOLOR        1
setenv LSCOLORS 
setenv CLICOLOR

set grey    = "%{\033[1;30m%}"
set red     = "%{\033[1;31m%}"
set green   = "%{\033[1;32m%}"
set yellow  = "%{\033[1;33m%}"
set blue    = "%{\033[1;34m%}"
set magenta = "%{\033[1;35m%}"
set cyan    = "%{\033[1;36m%}"
set white   = "%{\033[1;37m%}"
set end     = "%{\033[0m%}"

if ($tty =~ ttyv*) then
	if ( $USER == root ) then 
	set prompt = "${red}%N@%m ${white}%~ ${red}>>${end} "
	else set prompt = "${magenta}%N@%m ${white}%~ ${magenta}>${end} "
	endif
else
	if ( $USER == root ) then 
	set prompt = "${red}%N@%m ${white}%~ ${red}:${end} "
	else set prompt = "${magenta}%N@%m ${white}%~ ${magenta}:${end} "
	endif
endif


set complete = enhance
complete cd			'p/1/d/'
complete ping		'p/1/$hosts/'
complete service	'c/-/(e l r v)/' 'p/1/`service -l`/' 'n/*/(start stop reload restart status rcvar describe extracommands onestart onestop oneextracommands)/'
complete sysctl		'n/*/`sysctl -Na`/'
complete man		'C/*/c/'
complete netstat	'n@-I@`ifconfig -l`@'
complete doas		'p@1@`sed "s,.*cmd *\([^ ]*\).*,\1," /usr/local/etc/doas.conf`@'

# other autolist options
set		autolist = TAB

# start zoxide
eval `zoxide init posix --hook prompt`
