set expandtab
set shiftwidth=4
set softtabstop=4
set autoindent 
set smartindent
set cindent
if has ("autocmd")
    filetype plugin indent on
endif

set number
set wildmenu
set mouse=a
syntax on

nnoremap <C-q> :q!<CR>
nnoremap <C-s> :wq<CR>
nnoremap U :redo<CR>
filetype on
"colorscheme embark
colorscheme slate


