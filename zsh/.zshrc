# Most of the following settings come from vermaden blog https://vermaden.wordpress.com/2021/09/19/ghost-in-the-shell-part-7-zsh-setup
# Thanks to him. 

# ENVIRONMENT VARIABLES
typeset -U path
path+=(~/bin)

HISTSIZE=100000
SAVEHIST=100000
HISTFILE=$HOME/.zsh_history

export EDITOR="$(which vim)"
export TERM="xterm-256color"
export LANG=fr_FR.UTF-8
export PAGER="$(which less)"
export MANPATH=":/usr/share/man:/usr/local/man"
export MANPAGER="$(which less)"
export COLORTERM=truecolor

set -o emacs

# COMPLETION
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' completer _expand _complete _correct _approximate _history
zstyle ':completion:*' matcher-list '' '' 'l:|=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' users root
zstyle ':completion:*' menu select

# zmodload zsh/complist
autoload -Uz compinit
autoload -U colors && colors
compinit
_comp_options+=(globdots)

# KEYBINDINGS
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search
bindkey "^R" history-incremental-search-backward
bindkey "\e[A" history-beginning-search-backward
bindkey "\e[B" history-beginning-search-forward
bindkey '^[[Z' reverse-menu-complete
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line
bindkey "^K" kill-line

case "${TERM}" in
  (cons25*|linux)
    bindkey '\e[H'    beginning-of-line 
    bindkey '\e[F'    end-of-line      
    bindkey '\e[5~'   delete-char       
    bindkey '[D'      emacs-backward-word
    bindkey '[C'      emacs-forward-word
    ;;
  (*xterm*)
    bindkey '\e[H'    beginning-of-line
    bindkey '\e[F'    end-of-line     
    bindkey '\e[3~'   delete-char    
    bindkey '\e[1;5C' forward-word  
    bindkey '\e[1;5D' backward-word
    ;;
esac

# CURSOR MOTION
WORDCHARS='~!#$%^&*(){}[]<>?.+;-'
MOTION_WORDCHARS='~!#$%^&*(){}[]<>?.+;-/'

''{back,for}ward-word() WORDCHARS=$MOTION_WORDCHARS zle .$WIDGET
zle -N backward-word
zle -N forward-word

# ALIAS
alias ppi='pkg install'
alias pps='pkg search -o'
alias ppu='pkg update'
alias ppg='pkg upgrade'
alias ppr='pkg delete'
alias ppl='pkg info -l'
alias ppo='pkg info'
alias ppm='pkg info --pkg-message' 
alias ppoo='pkg search -D'
alias maj='ppu && ppg'

alias l='ls -1 --color=auto -F'
alias ll='ls -lh --color=auto -F'
alias la='ls -lha --color=auto -F'
alias lt='ls -lhrt --color=auto -F'
alias ld='ls -ld --color=auto -F'

alias cp='cp -v'
alias rm='rm -v'
alias rmf='rm -f -v'
alias rmd='rm -rf -v'
alias mv='mv -v'
alias bc='bc -q -l'
alias df='df -h'
alias du='du -hs'
alias v='$EDITOR'
alias cd-='cd -'
alias ping='ping -v -4 -c2 -t2'
alias ,='cd ../'
alias ,,='cd ../..'
alias eng_on='export LC_ALL=C'
alias eng_off='unset LC_ALL'

alias qe='qedit'
alias ql='qlook'
alias qg='qlog'
alias qb='qbackup'

# OPTIONS
setopt no_beep
setopt nohashdirs
setopt extended_glob
setopt auto_menu
setopt list_rows_first
setopt multios
setopt hist_ignore_all_dups
setopt append_history
setopt inc_append_history
setopt hist_reduce_blanks
setopt always_to_end
setopt no_hup
setopt complete_in_word
limit coredumpsize 0

# PROMPT
PROMPT_SUBT="$USER"
PROMPT="%F{#FD9A4F}${PROMPT_SUBT}%F{#F2D362}@%F{#CB296A}%M %F{#DEDEDE}%1~ %F{#F2D362}>%f "

# FUNCTIONS
src() { source ~/.zshenv }
qk() { ps -aux | sed 1d | fzy --line=25 | awk '{print $2}' | xargs kill -9 }

# LOAD ZOXIDE
eval "$(/usr/local/bin/zoxide init zsh)"
