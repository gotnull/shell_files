#!/usr/bin/env sh
# NAME: vmconf
# DESCRIPTION: settings for a freebsd vm
# DEPENDENCIES: none
# AUTHOR: gotnull
# LICENSE: Unlicense

set -eu
set -x

## create backup if zfs 
date_now=$(date "+%d%m%Y")
if df -hT | awk '/zroot/ {print $1}' ; then
    bectl create own_settings_"$date_now"
fi

## patch system
echo "--- PATCH LEVEL UPDATE ---"
env PAGER=/bin/cat /usr/sbin/freebsd-update --not-running-from-cron fetch install

## update packages  
echo "--- PACKAGE UPDATE ---"
# sh  export ASSUME_ALWAYS_YES=YES 
# csh setenv ASSUME_ALWAYS_YES "YES" 
export ASSUME_ALWAYS_YES=YES 
pkg bootstrap -y 
pkg update 
pkg upgrade -f -y

# packages
echo "--- PACKAGE INSTALL ---"
set -- openntpd doas vim zsh zoxide fzy tmux git-tiny
pkg install -y "$@"

## get personal repo
git clone https://framagit.org/gotnull/shell_files.git

## firewall
# IPFW
#sysrc firewall_enable="YES"
#sysrc firewall_allowservices="any"
#sysrc firewall_myservices="any"
#sysrc firewall_type="workstation"
#sysrc firewall_logdeny="YES"
#sysrc firewall_nat_enable="YES"

# PF
echo "--- FIREWALL ---"
cp ~/shell_files/pf/pf.conf /etc/
confirm1=""
while [ "$confirm1" != "y" ] && [ "$confirm1" != "Y" ]
do
	echo "Enter your network: " ; read -r network
	echo "Your choice is: $network"
	echo "Is that correct ? (y/n) " ; read -r confirm1
done
sed -i '' "s#netlocal = \"\"#netlocal = \"$network\"#" /etc/pf.conf

sysrc pf_enable="YES"
sysrc pf_rules="/etc/pf.conf"
sysrc pflog_enable="YES"
sysrc pflog_logfile="/var/log/pflog"
sysrc gateway_enable="yes"

## set password for rescue user 'toor'
echo "--- TOOR USER ---"
passwd toor

## configuring unprivileged user account
confirm2=""
while [ "$confirm2" != "y" ] && [ "$confirm2" != "Y" ]
do
	echo "Choose a user name for the new account: "	; read -r new_user_name
	echo "Your choice is: $new_user_name"
	echo "Is that correct ? (y/n) "	; read -r confirm2
done

pw user add -n "$new_user_name" -c 'simple user' -d /usr/home/"$new_user_name" -G wheel -m -s /bin/sh
passwd "$new_user_name"

## delete old user account
echo "--- OLD USER ---"
confirm3=""
confirm4=""
while true
do
	echo "Is there an old account to delete ? (y/n)" ; read -r confirm3
	case $confirm3 in
		y|Y) confirm4=""
		while [ "$confirm4" != "y" ] && [ "$confirm4" != "Y" ]
		do
			echo "What's the user account you want to delete" ; read -r user_to_delete
			echo "Your choice is: $user_to_delete"
			echo "Is that correct ? (y/n) " ; read -r confirm4
		done
		if getent passwd "$user_to_delete" >/dev/null
		then pw userdel -n "$user_to_delete" -r
		fi
		break
		;;
		n|N) echo "okay, let's continue!"
		break
		;;
		*) echo "Incorrect."
		;;
	esac
done

## various settings
echo "--- SETTINGS ---"
## locale
cp /usr/share/zoneinfo/Europe/Paris /etc/localtime

## quiet login
touch /root/.hushlogin

## ntp
sysrc openntp_enable="yes"

## boot time
echo 'autoboot_delay="1"' >> /boot/loader.conf

## sshd
sysrc sshd_enable="YES"

## doas
touch /usr/local/etc/doas.conf
echo "permit nopass keepenv :wheel" > /usr/local/etc/doas.conf

## default shell
chsh -s /usr/local/bin/zsh toor 
chsh -s /bin/sh root

## scripts 
cp ~/shell_files/scripts/q* /usr/local/bin/
cp ~/shell_files/scripts/t* /usr/local/bin/

## config root
cp ~/shell_files/zsh/.zshrc /root/
cp ~/shell_files/zsh/.zshenv /root/
cp ~/shell_files/sh/.shrc /root/
cp ~/shell_files/tmux/.tmux.conf /root/
cp ~/shell_files/vim/.vimrc /root/
cp -r ~/shell_files/vim/.vim /root/

sed 's|if \[ -x /usr/bin/fortune|#if \[ -x /usr/bin/fortune|' /root/.profile
sed 's|if ( -x /usr/bin/fortune|#if ( -x /usr/bin/fortune|' /root/.login

## config $new_user_name 
if [ -n $new_user_name ] ; then 
    chsh -s /bin/sh $new_user_name
    touch /usr/home/$new_user_name/.hushlogin
    cp ~/shell_files/sh/.shrc /usr/home/$new_user_name
    sed 's|if \[ -x /usr/bin/fortune|#if \[ -x /usr/bin/fortune|' /usr/home/$new_user_name/.profile
    sed 's|if ( -x /usr/bin/fortune|#if ( -x /usr/bin/fortune|' /usr/home/$new_user_name/.login
    chown $new_user_name:$new_user_name /usr/home/$new_user_name/.hushlogin
    chown $new_user_name:$new_user_name /usr/home/$new_user_name/.shrc
    chown $new_user_name:$new_user_name /usr/home/$new_user_name/.profile
    chown $new_user_name:$new_user_name /usr/home/$new_user_name/.login
fi

