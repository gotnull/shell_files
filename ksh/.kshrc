set -o emacs

# ENVIRONMENT VARIABLES
export PATH=$PATH:$HOME/bin
export HISTFILE=~/.ksh_history
export HISTSIZE=10000
export HISTCONTROL=ignoredups:ignorespace
export COLUMNS=300
export TERM=xterm-256color
export HOSTNAME=$(hostname)
export LANG=fr_FR.UTF-8
export EDITOR=$(which vim)
export VISUAL=$(which vim)

PAGER=$(which less)
export MANPATH=":/usr/share/man:/usr/local/man"
export MANPAGER=$(which less)

# ALIAS
alias ppi='pkg install'      
alias pps='pkg search'
alias ppu='pkg update'
alias ppg='pkg upgrade'
alias ppr='pkg delete'  
alias ppl='pkg info -l' 
alias ppo='pkg info'
alias ppm='pkg info --pkg-message' 
alias ppoo='pkg search -D'
alias maj='ppu && ppg'

alias l='ls -1'
alias ll='ls -lh'
alias la='ls -lha'
alias lt='ls -lhrt'
alias ld='ls -ld'

alias cp='cp -v'
alias rm='rm -v'
alias rmf='rm -f -v'
alias rmd='rm -rf -v'
alias mv='mv -v'
alias bc='bc -q -l'
alias df='df -h'
alias du='du -hs'
alias v='$EDITOR'
alias cd-='cd -'
alias ping='ping -v -4 -c2 -t2'
alias ,='cd ../'
alias ,,='cd ../..'
alias eng_on='export LC_ALL=C'
alias eng_off='unset LC_ALL'

# FUNCTIONS 
src() { 
    . "$HOME"/.kshrc 
}
qk() { 
    ps -aux | sed 1d | fzy --prompt="::> " --line=25 | awk '{print $2}' | xargs kill -9 
}

hst() {
    # from solene blog https://dataswamp.org/~solene/2021-10-17-ksh-fzf.html
    RES=$(fzy < $HISTFILE)
    test -n "$RES" || exit 0
    eval "$RES"
}
bind -m ^R=hst^J

# PROMPT
RST=$(echo -e "\033[00m")
BLK=$(echo -e "\033[30m")
RED=$(echo -e "\033[31m")
GRN=$(echo -e "\033[32m")
YEL=$(echo -e "\033[33m")
BLU=$(echo -e "\033[34m")
PUR=$(echo -e "\033[35m")
CYA=$(echo -e "\033[36m")
WHI=$(echo -e "\033[37m")
PS1='$(d=${PWD/#$HOME/"~"};printf %s "$BLU${d#${d%?/*/*/*}?/}") $YEL:$RST '

# CLEAR SCREEN
bind '^L=clear-screen'

# LOAD ZOXIDE
#eval "$(zoxide init posix --hook prompt)"
